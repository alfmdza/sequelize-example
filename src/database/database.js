import Sequelize from "sequelize";

export const sequelize = new Sequelize('project-db', 'postgres', 'postgres', {
    host: 'localhost',
    dialect: 'postgres'
});